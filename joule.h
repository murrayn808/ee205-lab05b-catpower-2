///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file joule.h
/// @version 1.0
///
/// @author Nathaniel Murray <murrayn@hawaii.edu>
/// @date 15_Feb_2022
//////////////////////////////////////////////////////////////////////////
const char JOULE         = 'j';
// A Joule represents the electricity required to run a 1 W device for 1 s


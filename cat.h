///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catpower.h
/// @version 1.0
///
/// @author Nathaniel Murray <murrayn@hawaii.edu>
/// @date 15_Feb_2022
//////////////////////////////////////////////////////////////////////////
#pragma once
const double CATPOWER_IN_A_JOULE       = 0 ;                // Cats do no work
const char CATPOWER      = 'c';

extern double fromCatPowerToJoule( double catpower );
extern double fromJouleToCatPower( double joule );

Energy converter
Usage:  catPower fromValue fromUnit toUnit
   fromValue: A number that we want to convert
   fromUnit:  The energy unit fromValue is in
   toUnit:  The energy unit to convert to

This program converts energy from one energy unit to another.
The units it can convert are: 
   j = Joule
   e = eV = electronVolt
   m = MT = megaton of TNT
   g = GGE = gasoline gallon equivalent
   f = foe = the amount of energy produced by a supernova
   c = catPower = like horsePower, but for cats

To convert from one energy unit to another, enter a number 
it's unit and then a unit to convert it to.  For example, to
convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g

